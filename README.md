# TEMA: "Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)" 

```plantuml 
@startmindmap 

 *[#40C4FF] Programación Declarativa. Orientaciones y pautas para el estudio
  *[#4FC3F7] Programacion
   *_ lenguajes 
    *[#43A49D] Codigo maquina
     *[#00B0FF] Mucha burocracia
     *[#00B0FF] Tareas sencillas
     *[#00B0FF] Creatividad relativamente nula
     *[#00B0FF] Gestion de memoria compleja
   *_ se dividen en
    *[#43A49D] Creativa
     *_ incluye 
      *[#B3E5FC] Ideas del algoritmo
      *[#B3E5FC] Ideas de como resolver un problema
      *[#B3E5FC] Forma de ahorrar tiempo
      *[#B3E5FC] Forma de ahorrar memoria
    *[#43A49D] burocratica
     *_ incluye
      *[#B3E5FC] Gestion de memoria del ordenador
      *[#B3E5FC] Secuencia de ordenes estrictas
      *[#B3E5FC] No se puede escapar de ella
  *[#4FC3F7] Problema de ordenacion
   *_ se resuelve
    *[#00B0FF] cada paso es importante para llegar al resultado final
  *[#4FC3F7] Programacion Imperativa
   *_ Mapa de Bits de la maquina
    *[#B3E5FC] Es una correspondencia que siempre debe de estar en la memoria de programados
     *_ caracteristicas
      *[#00B0FF] Es complejo de usar
      *[#00B0FF] produce errores con facilidad
      *[#00B0FF] es mas complicado de programar como en verdad deberia ser
   *_ ¿Que hace?
    *[#B3E5FC] Nos obliga a realizar una gestion detallada de la memoria 
   *_ Se basa en
    *[#B3E5FC] El modelo de Von Neumann
  *[#4FC3F7] Programacion Declarativa
   *_ comparacion 
    *[#4FC3F7] Arquitectura
     *_ Parte creativa
      *[#43A49D] Es la que se encarga de diseñar el edificios
     *_ Parte Burocratica
      *[#43A49D] Retiro de escombros de manera secuencial y estricta
   *_ se crean 
    *[#4FC3F7] Lenguajes de alto nivel
     *_ ejemplos
      *[#43A49D] Fortran 
       *_ programador 
        *[#B3E5FC] Siempre debe tener control detallado de la \nsecuencia de instrucciones asi como de la memoria ocupada
       *_ permitia
        *[#B3E5FC] Separarse del lenguaje primitivo que entiende el ordenador
       *_ se escribian 
        *[#B3E5FC] En lenguajes intermedios 
         *_ ocupan un
          *[#00B0FF] Compilador 
           *_ se encarga de:
            *[#00B0FF] traducir a ordenes primitivas que entendia el ordenador
   *_ al principio
    *[#4FC3F7] A los ordenadores se les hablaban en lenguaje maquina
     *_ programas
      *[#9FD7BF] Consistian en secuencias de ordenes
       *_ Directas
       *_ Concretas 
       *_ Muy sencillas
   *_ Consiste en:
    *[#4FC3F7] Las tareas rutinarias se le dejan al compilador
     *_ su proposito es:
      *[#43A49D] Reducir la complejidad de los programas
      *[#43A49D] Reducir el riesgo de cometer errores
      *[#43A49D] Reducir la faragosidad del codigo ya escrito
   *_ Conocida tambien como:
    *[#B3E5FC] Programacion Perezosa
     *_ Consiste 
      *[#43A49D] Solo ejecuta aquello que necesita para darnos respuesta
    *[#B3E5FC] Programacion Hermosa
    *[#B3E5FC] Programacion comoda
   *_ Se divide en:
    *[#B3E5FC] Programacion Funcional
     *_ se encarga de:
      *[#43A49D] Aplicar funciones a datos concretos
      *[#43A49D] Aplicar reglas de simplificacion
      *[#43A49D] Funciones de orden superior
     *_ libro 
      *[#B3E5FC] Jeremy Fokker
     *_ Permiten
      *_ Memorizacion 
       *[#B3E5FC] Almacena datos en memoria
      *_ Transparencia Referencial
       *[#B3E5FC] la salida de las funciones son independientes del orden 
      *_ Currificacion
       *[#B3E5FC] Regresan diferentes funciones como salida
      *_ evaluaciones
       *[#B3E5FC] Estricta
        *_ Funciona de dentro hacia afuera
       *[#B3E5FC] No Estricta
        *_ Funciona de fuera hacia dentro
       *[#B3E5FC] Perezosa
        *_ Evaluacion no repetitiva
     *_ ¿Que hace?
      *[#B3E5FC] Se encarga de recurrir al \nlenguaje que usan los matematicos y como definir funciones 
       *[#9FD7BF] Razonamiento ecuacional
        *_ ventajas
         *[#B3E5FC] Tiene caracteristicas esenciales y utiles \npara programar evaluacion perezosa
          *_ consiste en
           *[#00B0FF] Que solo se evaluan las ecuaciones, se calcula o computa \naquello que es estrictamente necesario para calculos posteriores
            *_ se definen
             *[#AACCCB] datos infinitos
              *_ por ejemplo
               *[#A0CEDE] Serie de Fibonacci         
         *[#B3E5FC] Usa funciones de orden superior
          *_ consiste en
           *[#00B0FF] Que una vez que tienes el combo de objetos principales y las funciones \npuedes definir funciones que actuen sobre funciones y no sobre datos primitivos
            *_ capacidad potente 
       *_ datos
        *[#9FD7BF] Un programa
         *_ tiene 
          *[#00B0FF] Datos de entrada
          *[#00B0FF] Funcion 
          *[#00B0FF] Datos de salida
    *[#B3E5FC] Programacion orientada e Inteligencia Artificial
     *[#43A49D] Lenguajes que estudian   
      *[#B3E5FC] Prolog
       *_ Se estudian
        *[#00B0FF] Conceptos basicos en \nrepresentacion de paradigma declarativa
       *[#43A49D] Es el lenguaje en el que se implementa el \nconcepto de programacion logica     
      *[#B3E5FC] Lisp
       *_ ¿Que es? 
        *[#43A49D] Es un lenguaje hibrido que permite realizar \nun tipo de programacion imperativa. Tiene relacion con la programacion funcional 
       *_ se estudia como
        *[#00B0FF] Un modelo de programacion imperativa 
    *[#B3E5FC] Programacion Funcional Pura
     *_ lenguaje estandar
      *[#B3E5FC] Haskell
       *_ se basa en:
        *[#43A49D] La evaluacion perezosa 
        *[#43A49D] Sistemas de diferencia de tipos
       *_ Sus compiladores son
        *_ Gofer
        *_ Hugs
    *[#B3E5FC] Programacion Logica
     *_ se basa
      *[#B3E5FC] Demostracion automatica
      *[#B3E5FC] En la inferencia 
     *_ compilador 
      *[#B3E5FC] Es un motor de inferencia que apartir de enuciados razona y da respuestas
     *_ consiste en
      *[#B3E5FC] el modelo de demostracion logica y automatica
       *_ se utilizan como:
        *[#00B0FF] Expresiones de los programas
         *_ Axiomas 
         *_ Reglas de inferencia 
   *_ Proposito
    *[#9FD7BF] Liberarse de las asignaciones y de tener que detallar \ncual es el control de memoria del ordenador
     *_ ventajas a conseguir
      *[#43A49D] Programas mas cortos 
      *[#43A49D] Faciles de realizar
      *[#43A49D] Faciles de depurar
   *_ ¿Que es?:
    *[#9FD7BF] Es un estilo de programacion(paradigma)
     *_ surge como
      *[#52B49B] Reaccion a algunos problemas que \nlleva la programacion clasica(imperativa)
       *_ Programador
        *[#43A49D] Se ve obligado a dar demasiados detalles \nsobre los calculos a realizar
         *_ instrumento \nesencial
          *[#94B4B3] Es la asignacion mediante la cual se modifica el estado de la memoria \n del ordenador y se modifica en detalle paso a paso
           *_ genera 
            *[#43A49D] Cuello de botella intelectual
       *_ Ejemplos:
        *[#9FD7BF]  Pascal
        *[#9FD7BF]  C++
        *[#9FD7BF]  Modulan
  *[#4FC3F7] Datos abstractos

@endmindmap 
```

# TEMA: "Lenguaje de Programación Funcional (2015)" 

```plantuml 
@startmindmap 
 
 *[#69F0AE] Lenguaje de Programación Funcional
  *[#B9F6CA] Programación Funcional 
   *[#C5E1A5] eficiencia 
    *_ consiste en
     *[#00E676] que tan rapido puedes escribir un programa 
   *_ año 1968
    *[#C5E1A5] acuño un termino
     *_ memorizacion 
      *[#00E676] almacenar el valor de una expresion cuya evaluacion ya fue realizada
     *_ quien 
      *[#00E676] donald michel 
   *_ como se evaluan 
  *[#B9F6CA] Evaluacion Perezosa 
   *_ nunca
    *[#C5E1A5] nunca trabaja de mas
    *[#C5E1A5] tiene buena memoria
    *[#C5E1A5] no vuelve a evaluar cosas ya evaluadas
  *[#B9F6CA] Evaluacion en corto circuito
   *_ consiste en 
    *[#C5E1A5] evaluacion muy costosa se realiza una unica vez
  *[#B9F6CA] Evaluacion no estricta
   *_ consiste en
    *[#C5E1A5] evaluar desde fuera hacia adentro
    *[#C5E1A5] no necesita saber el valor de alguno de sus parametros
  *[#B9F6CA] Evaluacion impaciente o estricta
   *_ sencilla de implementar
   *_ inconvenientes
    *[#C5E1A5] si no se puede evaluar una expresion en un tiempo finito
     *_ el calculo se interrumpe
     *_ se puede producir un error 
   *_ evalua
    *[#C5E1A5] las expresiones desde dentro hacia afuera
     *[#AED581] primero lo mas interno
     *[#AED581] evaluas lo externo despues de evaluar lo interno
   *_ tiene
    *[#C5E1A5] currificacion
     *_ devuelve
      *[#81C784] devuelve diferentes funciones como salidas
       *_ se le conoce como 
        *[#B9F6CA] aplicacion parcial
   *_ caracteristicas
    *[#C5E1A5] todo girar alrededor de las funciones
    *[#C5E1A5] todo son funciones
     *_ pueden ser
      *[#81C784] parametros de otras funciones
      *[#81C784] parametros de entrada
      *[#81C784] parametros de salida
     *_ se denominan
      *[#81C784] ciudadanos de primera clase
       *_ pueden ir en todas partes
   *_ bases
    *[#B9F6CA] Programa 
     *_ Coleccion de datos
     *_ serie de instrucciones
     *_ operan sobre datos
     *_ se ejecutan en un orden adecuado
     *_ el orden se modifica segun las variables
  *[#B9F6CA] Programacion Imperativa
   *_ concepto
    *[#B9F6CA] consiste en una secuencia claramente definida de instrucciones para un ordenador
  *[#B9F6CA] Paradigmas de programacion
   *_ consiste en 
    *[#00E676]  Un paradigma de programación es una manera o estilo de programación de software
  *[#B9F6CA] Lenguajes
   *_ basados en
    *[#B9F6CA] Modelo de computacion de Von Neumann
     *_ Propuso
      *[#00E676] Los programas debian almacenarce en la misma maquina 
       *_ caracteristicas
        *[#81C784] Serie de instruciones secuencialmente
        *[#81C784] Se puede modificar las instrucciones
        *_ memoria 
         *[#81C784] Se accede por medio de variables
         *[#81C784] Almacena todo el valor de todas las variables
          *_ se modifican mediante
           *[#81C784] Una instruccion de asignacion
   *[#C5E1A5] Lenguaje imperativo
    *_ se basa en
     *[#AED581] Darle ordenes a la maquina de manera secuencial
   *_ ¿Que es? 
    *[#00E676] Modelos de computacion de diferentes lenguajes
     *_ que hace
      *[#B9F6CA] Dotan de semantica a los programas
  *[#B9F6CA] Programacion Orientada a Objetos
   *_ tienen
    *[#81C784] Una serie de instrucciones secuencialmente
   *_ son
    *[#81C784] Pequeños trozos de codigo
    *[#81C784] Interactúan con otros codigos
    *[#81C784] Interactúan con objetos
  *[#B9F6CA] Lambda Calculo
   *_ seguido por
    *[#81C784] haskell boscurrie
     *_ desarrolla
      *[#00E676] La logica combinatoria 
       *_ cimientos 
        *[#00E676] Petter landie
         *_ modela un lenguaje de programacion
         *_ cimienta las bases de la programacion
         *_ año 1965
   *_ matematica
    *[#81C784] La definición depende de la funcion
   *_ computable
    *[#81C784] La defenicion no depende, los modelos no son distintos
   *_ se creo como
    *[#00E676] Un sistema para estudiar el concepto de funcion
     *_ Aplicacion de funciones
     *_ Recursividad
     *_ mas potente que el modelo de von neumman
    *[#00E676] Concepto basico todo lo que se puede hacer en uno se puede hacer en otro
   *_ se desarrollo
    *[#00E676] En la decada de 1930
     *_ por 
      *[#00E676] Alonzo Church 
      *[#00E676] Stephen Kleene 
  *[#B9F6CA] Logica Simbolica
   *_ Se llega a un
    *[#00E676] Paradigma de programacion logica
     *_ programa 
      *[#00E676] Se forma mediante un conjunto de sentencias
      *[#00E676] Definen lo que es verdad o conocido
      *[#00E676] Con respecto a un problema
@endmindmap 
```
# "Autor: Lopez Rodriguez Hector Aldahir" 

### Para la elaboracion de estos mapas conceptuales se usaron los formatos Mindmap y PlantUML .
#### Herramientas
##### PlantUML
[plantuml @ plantuml](https://docs.gitlab.com/ee/administration/integration/plantuml.html)
##### MindMap
[mindmap @ mindmap](https://plantuml.com/es/mindmap-diagram)



